# 使用教程

## debian or ubuntu
```shell
apt-get install curl -y
bash <(curl -s -L https://gitlab.com/x-x/ssr/-/raw/master/ssrmu.sh)
```

## centos7

```shell
yum install curl -y
bash <(curl -s -L https://gitlab.com/x-x/ssr/-/raw/master/ssrmu.sh)
```

## 系统优化脚本

```shell
bash <(curl -s -L https://git.io/optimize.sh)
```